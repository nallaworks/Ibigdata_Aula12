import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
headers = ['Preco','Data']
df = pd.read_csv('dados_filtrado.csv',names=headers)
print (df)

df['Data'] = df['Data'].map(lambda x: datetime.strptime(str(x)+' 00:00:00.0', '%Y/%m/%d %H:%M:%S.%f'))
x = df['Data']
y = df['Preco']

# plot
plt.plot(x,y)
# beautify the x-labels
plt.gcf().autofmt_xdate()

plt.show()