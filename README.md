# Aula 12 - Disciplina Introdução a BigData. 
## Objetivo
Coleta de dados - manipulação e Visualização.

## Desafio! 

Vamos visualizar um gráfico de cotações de soja

## Nosso problema:

* Capture 20 dias de cotações de soja
* Manipule o arquivo para remoção de erros e pontos discrepantes
* Gere o arquivo no formato proposto em aula
* Capture dados e gere gráficos de outras culturas.



## Perguntas:

- O que é um csv?

- O que é uma série histórica?


## Ambiente de Desenvolvimento

Ser possível executar um código em python.


a) acesso a internet.


## Recursos

Computadores com Linux.

### Como preparar o ambiente com Pyhton

GOOGLE!!!!

b)


### Como clonar o Projeto

`git clone gitlab.com/nallaworks/Ibigdata_Aula12.git`

## Entrega
Pessoal a entrega será: 

* Código com coleta de dados e gráfico.
* Onde mais posso aplicar essa técnica?

Grato, 
Allan